import textWidget from './textWidget'
import selectWidget from './selectWidget'
import pictureWidget from './pictureWidget'
import moneyWidget from './moneyWidget'
import attachmentWidget from './attachmentWidget'
import spanWidget from './spanWidget'
import buttonWidget from './buttonWidget'
import tableWidget from './tableWidget'
import containerWidget from './containerWidget'
import columnsWidget from './columnsWidget'
export default {
  textWidget,
  selectWidget,
  pictureWidget,
  moneyWidget,
  attachmentWidget,
  spanWidget,
  buttonWidget,
  tableWidget,
  containerWidget,
  columnsWidget
}