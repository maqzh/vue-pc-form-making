import KwTabs from './KwTabs'
import KwTabPane from './KwTabPane'
import KwAccordion from './KwAccordion'
import KwAccordionItem from './KwAccordionItem'
import KwDialog from './KwDialog'
import KwTree from './KwTree'
import KwCheckbox from './KwCheckbox'
import KwButtonGroup from './KwButtonGroup'
import KwButton from './KwButton'
import KwSelect from './KwSelect'
import KwTable from './KwTable'
import KwDropDown from './KwDropDown'

export default {
  KwTabs,
  KwTabPane,
  KwAccordion,
  KwAccordionItem,
  KwDialog,
  KwTree,
  KwCheckbox,
  KwButtonGroup,
  KwButton,
  KwSelect,
  KwTable,
  KwDropDown
}