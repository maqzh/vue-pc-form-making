export const generateID =(l) => {
  l = l || 32
  let str = ''
  do {str += 'x';l--;} while(l > 0);
  return str.replace(/[xy]/g, function(c) {
    let r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
    return v.toString(16);
  })
}

export const generateCID =() => {
  return 'cid_' + generateID()
}

export const queryDomByAttr = (parentNode, value, key) => {
  key = key || 'cid'
  return parentNode.querySelector('*[' + key + '=' + value + ']')
}

export const getComponentIndexByCID =(components, cid) => {
  let index = -1
  if (components) {
    components.every((o, i) => {
      if (o.cid === cid) {
        index = i
      }
      return index === -1
    })
  }
  return index
}

export const querySelectorChildren =(el, clsSelector) => {
  let domList = []
  let toogleSelectorChildren = (children) => {
    if (children) {
      for(let i = 0; i < children.length; i++) {
        let child = children[i]
        if (child.classList.contains(clsSelector)) {
          domList.push(child)
        } else {
          toogleSelectorChildren(child.children)
        }
      }
    }
  }
  toogleSelectorChildren(el.children)
  return domList
}