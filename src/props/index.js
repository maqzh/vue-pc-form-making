import commonProp from './commonProp'
import inputProp from './inputProp'
import textareaProp from './textareaProp'
import selectProp from './selectProp'
import optionsProp from './optionsProp'
import checkboxProp from './checkboxProp'
import columnProp from './columnProp'
import tableColumnProp from './tableColumnProp'

import databinderProp from './custom/databinderProp'
import privigeProp from './custom/privigeProp'

export default {
    commonProp,
    inputProp,
    textareaProp,
    selectProp,
    optionsProp,
    checkboxProp,
    columnProp,
    tableColumnProp,
    databinderProp,
    privigeProp
}