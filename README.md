# vue-pc-form-making

## 基于vue-mobile-form-making项目做电脑端的表单设计器。
https://gitee.com/maqzh/vue-mobile-form-making

## 设计器布局参照
https://gitee.com/GavinZhuLei/vue-form-making
```
vue-form-making也是很不棒的表单设计器
两个项目除布局像外，源码内容没有太多相关性。
```

## demo地址
http://form.kingwn.com/pcmaking/index.html

## 效果图
![image text](https://gitee.com/maqzh/vue-pc-form-making/raw/master/demo.jpg)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
